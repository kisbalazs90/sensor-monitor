<?php

namespace app\controllers;

use app\models\Device;
use app\models\LightMeasurement;
use app\models\search\LightMeasurementSearch;
use app\models\SensorData;

use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * LightMeasurementController implements the CRUD actions for Measurement model.
 */
class LightMeasurementController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LightMeasurement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LightMeasurementSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);
        $gridViewColumns = $searchModel->getGridViewColumns($queryParams);
        $deviceSelect2List = ArrayHelper::map(Device::find()->where(['type' => Device::ENUM_LIGHT])->all(), 'id', 'name');
        $sensorDataSelect2List = ArrayHelper::map(SensorData::find()->where(['type' => Device::ENUM_LIGHT])->all(), 'id', 'name');
        return $this->render('index', [
            'action' => 'index',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'gridViewColumns' => $gridViewColumns,
            'deviceSelect2List' => $deviceSelect2List,
            'sensorDataSelect2List' => $sensorDataSelect2List,
        ]);
    }

    /**
     * Displays a single LightMeasurement model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LightMeasurement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LightMeasurement();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Measurement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Measurement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

        /**
     * Finds the Measurement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionLightStat()
    {
        $title = 'Fénymérés statisztika (PAR)';
        $searchModel = new LightMeasurementSearch();
        $qureyParams = Yii::$app->request->queryParams;
        $meassurements = $searchModel->searchForCharts($qureyParams);
        $deviceSelect2List = ArrayHelper::map(Device::find()->where(['type' => Device::ENUM_LIGHT])->all(), 'id', 'name');
        $sensorDataSelect2List = ArrayHelper::map(SensorData::find()->where(['type' => SensorData::ENUM_LIGHT])->all(), 'id', 'name');
        $deviceTags = [];

        if (!empty($qureyParams['LightMeasurementSearch']['device_id'])) {
            $createdAtStart = $qureyParams['LightMeasurementSearch']['created_at_start'];
            $createdAtEnd = $qureyParams['LightMeasurementSearch']['created_at_end'];
            $title.= $createdAtStart . ' és '.$createdAtEnd .' között';

            $i = 1;
            $sensorData = $meassurements['sensorData'];

            foreach ($sensorData as $sensor) {
                $deviceTags[] = [
                    'device_color' => Yii::$app->params['deviceColors'][$i],
                    'device_name' => $sensor->name.' ('.$sensor->unit ? $sensor->unit : 'Hiányzó mértékegység'.')',
                ];
                $i++;
            }
        }

        return $this->render('stat', [
            'title' => $title,
            'action' => 'light-stat',
            'meassurements' => json_encode($meassurements),
            'searchModel' => $searchModel,
            'deviceSelect2List' => $deviceSelect2List,
            'sensorDataSelect2List' => $sensorDataSelect2List,
            'deviceTags' => $deviceTags,
            'sumOfPar' => $meassurements['sumOfPar'],
        ]);
    }

    /**
     * Finds the Measurement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionDliStat()
    {
        $title = 'Fénymérés statisztika (DLI) ';
        $searchModel = new LightMeasurementSearch();
        $qureyParams = Yii::$app->request->queryParams;
        $meassurements = $searchModel->searchForDliCharts($qureyParams);
        $deviceSelect2List = ArrayHelper::map(Device::find()->where(['type' => Device::ENUM_LIGHT])->all(), 'id', 'name');
        $sensorDataSelect2List = ArrayHelper::map(SensorData::find()->where(['type' => SensorData::ENUM_LIGHT])->all(), 'id', 'name');
        $deviceTags = [];

        if (!empty($qureyParams['LightMeasurementSearch']['device_id'])) {
            $createdAtStart = $qureyParams['LightMeasurementSearch']['created_at_start'];
            $createdAtEnd = $qureyParams['LightMeasurementSearch']['created_at_end'];
            $createdAtStart = date("Y-m-d", strtotime($createdAtStart));
            $createdAtEnd = date("Y-m-d", strtotime($createdAtEnd));
            $title.= $createdAtStart . ' és '.$createdAtEnd .' között';

            $i = 1;
            $sensorData = $meassurements['sensorData'];

            foreach ($sensorData as $sensor) {
                $deviceTags[] = [
                    'device_color' => Yii::$app->params['deviceColors'][$i],
                    'device_name' => $sensor->name.' ('.$sensor->unit ? $sensor->unit : 'Hiányzó mértékegység'.')',
                ];
                $i++;
            }
        }

        return $this->render('dli-stat', [
            'title' => $title,
            'action' => 'dli-stat',
            'meassurements' => json_encode($meassurements),
            'searchModel' => $searchModel,
            'deviceSelect2List' => $deviceSelect2List,
            'sensorDataSelect2List' => $sensorDataSelect2List,
            'deviceTags' => $deviceTags,
            'sumOfPar' => $meassurements['sumOfPar'],
        ]);
    }

    public function actionDliStatDaily()
    {

        $title = 'DLI napi statisztika';
        $searchModel = new LightMeasurementSearch();
        $qureyParams = Yii::$app->request->queryParams;
        $meassurements = $searchModel->searchForDliDailyCharts($qureyParams);
        $deviceSelect2List = ArrayHelper::map(Device::find()->where(['type' => Device::ENUM_LIGHT])->all(), 'id', 'name');
        $deviceTags = [];

        if (!empty($qureyParams['LightMeasurementSearch']['device_id'])) {
            $createdAt = $qureyParams['LightMeasurementSearch']['created_at'];
            $title.= $createdAt.' napon';

            $i = 1;
            $deviceTags[] = [
                'device_color' => Yii::$app->params['deviceColors'][$i],
                'device_name' => 'DLI',
            ];
        }

        return $this->render('dli-stat-daily', [
            'title' => $title,
            'action' => 'dli-stat-daily',
            'meassurements' => json_encode($meassurements),
            'searchModel' => $searchModel,
            'deviceSelect2List' => $deviceSelect2List,
            'deviceTags' => $deviceTags,
            'sumOfPar' => $meassurements['sumOfPar'],
        ]);
    }

    public function actionGroupsStat()
    {
        $title = 'Hőmérséklet / Nyomás adott időben';
        $searchModel = new LightMeasurementSearch();
        $qureyParams = Yii::$app->request->queryParams;
        $deviceSelect2List = ArrayHelper::map(Device::find()->where(['type' => Device::ENUM_LIGHT])->all(), 'id', 'name');
        $deviceTags = [];
        $meassurements = '';

        if (!empty($qureyParams['MeasurementSearch']['type_of_stat'])) {
            if ($qureyParams['MeasurementSearch']['type_of_stat'] == 'pressure_in') {
                $meassurements = $searchModel->searchForCharts($qureyParams, 'data3');
                $title = 'Bejövő nyomás - '. $qureyParams['MeasurementSearch']['created_at'];
            } else if ($qureyParams['MeasurementSearch']['type_of_stat'] == 'pressure_out') {
                $meassurements = $searchModel->searchForCharts($qureyParams, 'data4');
                $title = 'Bejövő nyomás - '. $qureyParams['MeasurementSearch']['created_at'];
            } else if ($qureyParams['MeasurementSearch']['type_of_stat'] == 'temperature_in') {
                $meassurements = $searchModel->searchForCharts($qureyParams, 'data1');
                $title = 'Bejövő hőmérséklet - '. $qureyParams['MeasurementSearch']['created_at'];
            } else {
                $meassurements = $searchModel->searchForCharts($qureyParams, 'data2');
                $title = 'Kimenő hőmérséklet - '. $qureyParams['MeasurementSearch']['created_at'];
            }
        }

        if (!empty($meassurements['meassurementData'])) {

            foreach ($meassurements['meassurementData'] as $count => $details) {
                $meassurements['meassurementData'][$count]['data'] = [$details['data'][0]];
            }

            $meassurements['bottomLabels'] = explode(',', $meassurements['bottomLabels'])[0];
        }

        if (!empty($qureyParams['MeasurementSearch']['device_id'])) {
            $devices = $meassurements['devices'];
            $i = 1;
            if ($devices) {
                foreach ($devices as $deviceId) {
                    $deviceModel = Device::findOne($deviceId);

                    if (!$deviceModel) {
                        continue;
                    }

                    $deviceTags[] = [
                        'device_color' => Yii::$app->params['deviceColors'][$i],
                        'device_name' => $deviceModel->name
                    ];

                    $i++;
                }
            }
        }

        return $this->render('groups-stat', [
            'title' => $title,
            'action' => 'groups-stat',
            'meassurements' => json_encode($meassurements),
            'searchModel' => $searchModel,
            'deviceSelect2List' => $deviceSelect2List,
            'deviceTags' => $deviceTags,
        ]);
    }


    protected function findModel($id)
    {
        if (($model = LightMeasurement::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
