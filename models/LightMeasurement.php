<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "light_measurement".
 *
 * @property int $id
 * @property int $device_id
 * @property string $data1
 * @property string $data2
 * @property string $data3
 * @property string $data4
 * @property string $data5
 * @property string $data6
 * @property string $data7
 * @property string $created_at
 */
class LightMeasurement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'light_measurement';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_id'], 'integer'],
            [['created_at'], 'safe'],
            [['data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_id' => 'Device ID',
            'data1' => 'Data1',
            'data2' => 'Data2',
            'data3' => 'Data3',
            'data4' => 'Data4',
            'data5' => 'Data5',
            'data6' => 'Data6',
            'data7' => 'Data7',
            'created_at' => 'Létrehozva',
        ];
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevice() {
        return $this->hasOne(Device::className(), ['id' => 'device_id']);
    }

}
