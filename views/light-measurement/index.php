<?php

use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LightMeasurementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fény Mérések';
$this->params['breadcrumbs'][] = $this->title;
\app\assets\MeasurementAsset::register($this);
?>

<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-md-12">
                <?php
                echo Yii::$app->controller->renderPartial('partials/_search', [
                    'action' => $action,
                    'model' => $searchModel,
                    'deviceSelect2List' => $deviceSelect2List,
                    'sensorDataSelect2List' => $sensorDataSelect2List
                ]);
                ?>
            </div>
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                    </div>
                    <div class="box-body">
                        <?=
                        GridView::widget([
                            'summary' => false,
                            'dataProvider' => $dataProvider,
                            'columns' => $gridViewColumns,
                            'export' => [
                                'label' => 'Adatok exportálása',
                            ],
                            'exportConfig' => [
                                GridView::CSV => [],
                                GridView::HTML => [],
                                GridView::PDF => [],
                            ],
                            'toolbar' => [
                                [
                                    'content' =>
                                        Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                                            'type' => 'button',
                                            'title' => 'Add Book',
                                            'class' => 'btn btn-success'
                                        ]) . ' ' .
                                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], [
                                            'class' => 'btn btn-default',
                                            'title' => 'Reset Grid',
                                        ]),
                                    'options' => ['class' => 'btn-group-sm']
                                ],
                                '{export}',
                                '{toggleData}'
                            ],
                            'toggleDataContainer' => ['class' => 'btn-group-sm'],
                            'exportContainer' => ['class' => 'btn-group-sm'],
                            'layout' => '{summary}{export}{items}{pager}',
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
