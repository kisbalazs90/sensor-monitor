<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sensor_data".
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $json_name
 * @property string $unit
 * @property string $created_at
 * @property int $is_disabled
 *
 * @property Measurement[] $measurements
 */
class SensorData extends \yii\db\ActiveRecord
{
    const ENUM_TEMP = 'TEMP';
    const ENUM_LIGHT = 'LIGHT';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sensor_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_disabled'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['unit'], 'string', 'max' => 255],
            [['json_name', 'type'], 'string', 'max' => 5],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Azonosító',
            'name' => 'Név',
            'json_name' => 'JSON név',
            'unit' => 'Mértékegység',
            'created_at' => 'Létrehozva',
            'is_disabled' => 'Inaktív',
            'type' => 'Típus'
        ];
    }
}
