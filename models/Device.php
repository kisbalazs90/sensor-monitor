<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "device".
 *
 * @property int $id
 * @property int $place_id
 * @property string $name
 * @property string $serial
 * @property string $created_at
 * @property string $type
 * @property Place $place
 * @property SensorData[] $sensorDatas
 */
class Device extends \yii\db\ActiveRecord
{
    const ENUM_TEMP = 'TEMP';
    const ENUM_LIGHT = 'LIGHT';
    const MAX_MINUTE_BETWEEN_SENDS = 15;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'device';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['place_id'], 'integer'],
            [['created_at'], 'safe'],
            [['name', 'serial', 'type'], 'string', 'max' => 255],
            [['place_id', 'name', 'serial'], 'required'],

            [['place_id'], 'exist', 'skipOnError' => true, 'targetClass' => Place::className(), 'targetAttribute' => ['place_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Azonosító',
            'place_id' => 'Mérőhely azonosító',
            'name' => 'Név',
            'serial' => 'Serial azonosító',
            'created_at' => 'Létrehozva',
            'type' => 'Típus',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(Place::className(), ['id' => 'place_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSensorDatas()
    {
        return $this->hasMany(SensorData::className(), ['device_id' => 'id']);
    }
    
    public static function getStatus($device) {
        if (!$device->id) {
            return;
        }
        
        $lastMeasurement = (new Measurement())->getLastMeasurement($device->id);
        
        if (empty($lastMeasurement)) {
            return ['class' => 'danger'];
        }

        date_default_timezone_set('Europe/Budapest');
        
        $date1 = strtotime($lastMeasurement->created_at);
        $date2 = strtotime(date('Y-m-d H:i:s'));

        if ((($date2 - $date1) / 60) > Device::MAX_MINUTE_BETWEEN_SENDS) {
            return ['class' => 'danger'];
        }

        return ['class' => 'success'];
    }
          
}
