<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Device;

/**
 * DeviceSearch represents the model behind the search form of `app\models\Device`.
 */
class DeviceSearch extends Device {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['id', 'place_id'], 'integer'],
                [['name', 'serial', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Device::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['name' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'place_id' => $this->place_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'serial', $this->serial]);

        return $dataProvider;
    }

    public function getColumns() {

        $columnDetails = [];

        $sensorData = \app\models\SensorData::find()->all();

        foreach ($sensorData as $data) {
            $columnDetails[$data->json_name]['label'] = $data->name;
            $columnDetails[$data->json_name]['visible'] = $data->is_disabled ? false : true;
            $columnDetails[$data->json_name]['unit'] = $data->unit;
        }
        
        $columns = [
                ['class' => 'yii\grid\SerialColumn'],
                [
                'attribute' => 'name',
                'label' => 'Mérőhely',
                'value' => function ($model) {
                    return $model->place ? $model->place->name : '';
                }
            ],
                [
                'attribute' => 'name',
                'label' => 'Eszköz',
                'value' => function ($model) {
                    return $model->name ? $model->name : '';
                }
            ],
                [
                'attribute' => 'id',
                'label' => 'Utolsó mérés időpontja',
                'value' => function ($model) {
                    $lastMeasurement = \app\models\Measurement::find()->where(['device_id' => $model->id])->orderBy(['created_at' => SORT_DESC])->one();
                    return !empty($lastMeasurement) ? $lastMeasurement->created_at : '';
                },
                
            ],                    
                [
                'attribute' => 'id',
                'label' => $columnDetails['data1']['label'],
                'value' => function ($model) use ($columnDetails) {
                    $lastMeasurement = \app\models\Measurement::find()->where(['device_id' => $model->id])->orderBy(['created_at' => SORT_DESC])->one();
                    return !empty($lastMeasurement) ? $lastMeasurement->data1 . $columnDetails['data1']['unit'] : '';
                },
                'visible' => $columnDetails['data1']['visible']
            ],
                [
                'attribute' => 'id',
                'label' => $columnDetails['data2']['label'],
                'value' => function ($model) use ($columnDetails) {
                    $lastMeasurement = \app\models\Measurement::find()->where(['device_id' => $model->id])->orderBy(['created_at' => SORT_DESC])->one();
                    return !empty($lastMeasurement) ? $lastMeasurement->data2 . $columnDetails['data2']['unit'] : '';
                },
                'visible' => $columnDetails['data2']['visible']
            ],
                [
                'attribute' => 'id',
                'label' => $columnDetails['data3']['label'],
                'value' => function ($model) use ($columnDetails) {
                    $lastMeasurement = \app\models\Measurement::find()->where(['device_id' => $model->id])->orderBy(['created_at' => SORT_DESC])->one();
                    return !empty($lastMeasurement) ? $lastMeasurement->data3 . $columnDetails['data3']['unit'] : '';
                },
                'visible' => $columnDetails['data3']['visible']
            ],
                [
                'attribute' => 'id',
                'label' => $columnDetails['data4']['label'],
                'value' => function ($model) use ($columnDetails) {
                    $lastMeasurement = \app\models\Measurement::find()->where(['device_id' => $model->id])->orderBy(['created_at' => SORT_DESC])->one();
                    return !empty($lastMeasurement) ? $lastMeasurement->data4 . $columnDetails['data4']['unit'] : '';
                },
                'visible' => $columnDetails['data4']['visible']
            ],
        ];

        return $columns;
    }

}
