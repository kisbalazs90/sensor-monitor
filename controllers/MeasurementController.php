<?php

namespace app\controllers;

use app\models\Device;
use app\models\Measurement;
use app\models\search\MeasurementSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * MeasurementController implements the CRUD actions for Measurement model.
 */
class MeasurementController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Measurement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MeasurementSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);
        $gridViewColumns = $searchModel->getGridViewColumns();
        $deviceSelect2List = ArrayHelper::map(Device::find()->where(['type' => Device::ENUM_TEMP])->orderBy('name')->all(), 'id', 'name');

        return $this->render('index', [
            'action' => 'index',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'gridViewColumns' => $gridViewColumns,
            'deviceSelect2List' => $deviceSelect2List
        ]);
    }

    /**
     * Displays a single Measurement model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Measurement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Measurement();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Measurement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Measurement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Measurement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionMeassureStat()
    {
        $title = 'Hőmérséklet statisztika (celsius) ';
        $searchModel = new MeasurementSearch();
        $qureyParams = Yii::$app->request->queryParams;
        $meassurements = $searchModel->searchForCharts($qureyParams, 'data1', 'data2');
        $deviceSelect2List = ArrayHelper::map(Device::find()->where(['type' => Device::ENUM_TEMP])->all(), 'id', 'name');
        $deviceTags = [];

        if (!empty($qureyParams['MeasurementSearch']['device_id'])) {
            $createdAtStart = $qureyParams['MeasurementSearch']['created_at_start'];
            $createdAtEnd = $qureyParams['MeasurementSearch']['created_at_end'];
            $title.= $createdAtStart .' és '.$createdAtEnd .' között';


            $i = 1;
            $devices = $meassurements['devices'];
            if ($devices)  {
                foreach ($devices as $deviceId) {
                    $deviceModel = Device::findOne($deviceId);

                    if (!$deviceModel) {
                        continue;
                    }

                    for ($j = 0; $j < 2; $j++) {
                        $deviceTags[] = [
                            'device_color' => Yii::$app->params['deviceColors'][$i],
                            'device_name' => $i % 2 == 0 ? $deviceModel->name . "- kimenő" : $deviceModel->name . "- bejövő"
                        ];

                        $i++;
                    }
                }
            }
        }

        return $this->render('stat', [
            'title' => $title,
            'action' => 'meassure-stat',
            'meassurements' => json_encode($meassurements),
            'searchModel' => $searchModel,
            'deviceSelect2List' => $deviceSelect2List,
            'deviceTags' => $deviceTags,
        ]);
    }


    public function actionPressureStat()
    {
        $title = 'Nyomás statisztika (bar) ';
        $searchModel = new MeasurementSearch();
        $qureyParams = Yii::$app->request->queryParams;
        $meassurements = $searchModel->searchForCharts($qureyParams, 'data3', 'data4');
        $deviceSelect2List = ArrayHelper::map(Device::find()->where(['type' => Device::ENUM_TEMP])->all(), 'id', 'name');
        $deviceTags = [];

        if (!empty($qureyParams['MeasurementSearch']['device_id'])) {
            $createdAtStart = $qureyParams['MeasurementSearch']['created_at_start'];
            $createdAtEnd = $qureyParams['MeasurementSearch']['created_at_end'];
            $title.= $createdAtStart .' és '.$createdAtEnd .' között';

            $i = 1;
            $devices = $meassurements['devices'];

            if ($devices)  {
                foreach ($devices as $deviceId) {
                    $deviceModel = Device::findOne($deviceId);

                    if (!$deviceModel) {
                        continue;
                    }

                    for ($j = 0; $j < 2; $j++) {
                        $deviceTags[] = [
                            'device_color' => Yii::$app->params['deviceColors'][$i],
                            'device_name' => $i % 2 == 0 ? $deviceModel->name . "- kimenő" : $deviceModel->name . "- bejövő"
                        ];

                        $i++;
                    }

                        $i++;
                }
            }
        }

        return $this->render('stat', [
            'title' => $title,
            'action' => 'pressure-stat',
            'meassurements' => json_encode($meassurements),
            'searchModel' => $searchModel,
            'deviceSelect2List' => $deviceSelect2List,
            'deviceTags' => $deviceTags,
        ]);
    }

    public function actionGroupsStat()
    {
        $title = 'Hőmérséklet / Nyomás adott időben';
        $searchModel = new MeasurementSearch();
        $qureyParams = Yii::$app->request->queryParams;
        $deviceSelect2List = ArrayHelper::map(Device::find()->where(['type' => Device::ENUM_TEMP])->all(), 'id', 'name');
        $deviceTags = [];
        $meassurements = '';

        if (!empty($qureyParams['MeasurementSearch']['type_of_stat'])) {
            if ($qureyParams['MeasurementSearch']['type_of_stat'] == 'pressure_in') {
                $meassurements = $searchModel->searchForCharts($qureyParams, 'data3');
                $title = 'Bejövő nyomás - '. $qureyParams['MeasurementSearch']['created_at'];
            } else if ($qureyParams['MeasurementSearch']['type_of_stat'] == 'pressure_out') {
                $meassurements = $searchModel->searchForCharts($qureyParams, 'data4');
                $title = 'Bejövő nyomás - '. $qureyParams['MeasurementSearch']['created_at'];
            } else if ($qureyParams['MeasurementSearch']['type_of_stat'] == 'temperature_in') {
                $meassurements = $searchModel->searchForCharts($qureyParams, 'data1');
                $title = 'Bejövő hőmérséklet - '. $qureyParams['MeasurementSearch']['created_at'];
            } else {
                $meassurements = $searchModel->searchForCharts($qureyParams, 'data2');
                $title = 'Kimenő hőmérséklet - '. $qureyParams['MeasurementSearch']['created_at'];
            }
        }

        if (!empty($meassurements['meassurementData'])) {
                   
            foreach ($meassurements['meassurementData'] as $count => $details) {
                $meassurements['meassurementData'][$count]['data'] = [$details['data'][0]];
            }

            $meassurements['bottomLabels'] = explode(',', $meassurements['bottomLabels'])[0];

        }
        if (!empty($qureyParams['MeasurementSearch']['device_id'])) {
            $devices = $meassurements['devices'];
            $i = 1;
            if ($devices) {
                foreach ($devices as $deviceId) {
                    $deviceModel = Device::findOne($deviceId);

                    if (!$deviceModel) {
                        continue;
                    }

                    $deviceTags[] = [
                        'device_color' => Yii::$app->params['deviceColors'][$i],
                        'device_name' => $deviceModel->name
                    ];

                    $i++;
                }
            }
        }

        return $this->render('groups-stat', [
            'title' => $title,
            'action' => 'groups-stat',
            'meassurements' => json_encode($meassurements),
            'searchModel' => $searchModel,
            'deviceSelect2List' => $deviceSelect2List,
            'deviceTags' => $deviceTags,
        ]);
    }


    protected function findModel($id)
    {
        if (($model = Measurement::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
