<?php

use yii\db\Migration;

/**
 * Class m180114_135901_alter_table_measurements
 */
class m180114_135901_alter_table_measurements extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->dropTable('measurement');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');

        $this->createTable('measurement', [
            'id' => $this->primaryKey(),
            'device_id' => $this->integer(),
        ], 'Engine=InnoDB');

        $this->addColumn('measurement', 'data1', $this->string(10));
        $this->addColumn('measurement', 'data2', $this->string(10));
        $this->addColumn('measurement', 'data3', $this->string(10));
        $this->addColumn('measurement', 'data4', $this->string(10));
        $this->addColumn('measurement', 'data5', $this->string(10));
        $this->addColumn('measurement', 'created_at', $this->timestamp());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180114_135901_alter_table_measurements cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180114_135901_alter_table_measurements cannot be reverted.\n";

        return false;
    }
    */
}
