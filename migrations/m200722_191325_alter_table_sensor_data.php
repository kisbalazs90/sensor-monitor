<?php

use yii\db\Migration;

/**
 * Class m200722_191325_alter_table_sensor_data
 */
class m200722_191325_alter_table_sensor_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('sensor_data', 'unit', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200722_191325_alter_table_sensor_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200722_191325_alter_table_sensor_data cannot be reverted.\n";

        return false;
    }
    */
}
