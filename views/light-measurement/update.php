<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LightMeasurement */

$this->title = 'Update Light Measurement: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Light Measurements', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="light-measurement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
