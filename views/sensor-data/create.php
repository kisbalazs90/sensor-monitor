<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SensorData */

$this->title = Yii::t('app', 'Create Sensor Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sensor Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sensor-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
