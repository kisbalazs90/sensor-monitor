<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PlaceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<div class="place-index">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>Eszközök állapota</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?=
                                    GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'filterModel' => $searchModel,
                                        'rowOptions' => function ($model) {
                                            return app\models\Device::getStatus($model);
                                        },
                                        'columns' => $gridViewColumns,
                                    ]);
                                    ?>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.box -->
</div>
