<?php

use yii\db\Migration;

/**
 * Class m200629_131301_insert_into_sensorData
 */
class m200629_131301_insert_into_sensorData extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       $this->delete('sensor_data', ['type' => 'LIGHT']);
       $this->execute("INSERT INTO sensor_data (name, json_name, unit, is_disabled, `type`) VALUES ('data1','data1','',0,'LIGHT');");
       $this->execute("INSERT INTO sensor_data (name, json_name, unit, is_disabled, `type`) VALUES ('data2','data2','',0,'LIGHT');");
       $this->execute("INSERT INTO sensor_data (name, json_name, unit, is_disabled, `type`) VALUES ('data3','data3','',0,'LIGHT');");
       $this->execute("INSERT INTO sensor_data (name, json_name, unit, is_disabled, `type`) VALUES ('data4','data4','',0,'LIGHT');");
       $this->execute("INSERT INTO sensor_data (name, json_name, unit, is_disabled, `type`) VALUES ('data5','data5','',1,'LIGHT');");
       $this->execute("INSERT INTO sensor_data (name, json_name, unit, is_disabled, `type`) VALUES ('data6','data6','',1,'LIGHT');");
       $this->execute("INSERT INTO sensor_data (name, json_name, unit, is_disabled, `type`) VALUES ('data7','data7','',1,'LIGHT');");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200629_131301_insert_into_sensorData cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200629_131301_insert_into_sensorData cannot be reverted.\n";

        return false;
    }
    */
}
