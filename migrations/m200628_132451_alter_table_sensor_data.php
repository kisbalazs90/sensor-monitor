<?php

use yii\db\Migration;

/**
 * Class m200628_132451_alter_table_sensor_data
 */
class m200628_132451_alter_table_sensor_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("SET FOREIGN_KEY_CHECKS = 0;");
        $this->dropForeignKey('sensor_data_ibfk_1', 'sensor_data');
        $this->dropColumn('sensor_data', 'device_id');
        $this->dropColumn('sensor_data', 'direction');
        $this->execute("SET FOREIGN_KEY_CHECKS = 1;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200628_132451_alter_table_sensor_data cannot be reverted.\n";

        return false;
    }
    */
}
