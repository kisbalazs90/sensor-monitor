<?php

use yii\db\Migration;

/**
 * Class m200709_193035_alter_table_sensor_data
 */
class m200709_193035_alter_table_sensor_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200709_193035_alter_table_sensor_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200709_193035_alter_table_sensor_data cannot be reverted.\n";

        return false;
    }
    */
}
