<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\MeasurementSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .s2-togall-button {
        display: none !important;
    }
</style>

<div class="measurement-search">

    <?php
    $form = ActiveForm::begin([
        'action' => [$action],
        'method' => 'get',
    ]);
    ?>

    <?php
    $model = new app\models\search\LightMeasurementSearch();
    ?>

    <div class="row">
        <div class="col-md-2">
            <label for="device_id">Eszköz kiválasztása</label>
            <?=
            $form->field($model, 'device_id')->widget('\kartik\select2\Select2', [
                'data' => $deviceSelect2List,
                'options' => [
                    'showToggleAll' => false,
                    'placeholder' => 'Válassz...',
                    'multiple' => false,
                ],
                'pluginOptions' => [
                    'maximumSelectionLength'=> 5,
                ],
            ])->label(false)
            ?>
        </div>
        
        <div class="col-md-3">
            <label for="device_id">Mért adatok</label>
            <?=
            $form->field($model, 'sensor_param')->widget('\kartik\select2\Select2', [
                'data' => $sensorDataSelect2List,
                'options' => [
                    'showToggleAll' => false,
                    'placeholder' => 'Válassz...',
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'maximumSelectionLength'=> 10,
                ],
            ])->label(false)
            ?>
        </div>

        <div class="col-md-3">
            <label for="created_at_start">Kezdő dátum</label>
            <?=
            $form->field($model, 'created_at_start')->widget('\kartik\datetime\DateTimePicker', [
                    'name' => 'created_at',
                    'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d H:i:s'),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii'
                    ]
                ]
            )->label(false)
            ?>
        </div>

        <div class="col-md-3">
            <label for="created_at_start">Végső dátum</label>
            <?=
            $form->field($model, 'created_at_end')->widget('\kartik\datetime\DateTimePicker', [
                    'name' => 'created_at',
                    'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d H:i:s'),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii'
                    ]
                ]
            )->label(false)
            ?>
        </div>

        <div class="col-md-1 no-padding">
            <label for="">&nbsp;</label>
            <div class="form-group">
                <?= Html::submitButton('Keresés', ['class' => 'btn btn-primary']) ?>
            </div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
