<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;

/**
 * This command removes old datas from database.
 *
 * @author Balázs Kis <kisbalazs90@gmail.com>
 * @since 2.0
 */
class CleanController extends Controller
{
    /**
     * Erase meassurement and lightMeassurement tables.
     * @param string $message the message to be echoed.
     */
    public function actionRemoveOldData()
    {
        $currentMinusOneYear = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')." -1 year"));
        
        $heatMeassureModel = new \app\models\Measurement();
        $countOfHeatDelete = 0;
        $heatMeassureModel->deleteAll(['between', 'created_at', '2000-01-01 00:00:00', $currentMinusOneYear]);

        $lightMeassureModel = new \app\models\LightMeasurement();
        $lightMeassureModel->deleteAll(['between', 'created_at', '2000-01-01 00:00:00', $currentMinusOneYear]);
    }
}
