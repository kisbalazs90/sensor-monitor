<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class   ChartAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'AdminLTE/bower_components/jquery/dist/jquery.min.js',
        'AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js',
        'AdminLTE/bower_components/chart.js/Chart.js',
        'AdminLTE/bower_components/fastclick/lib/fastclick.js',
        'AdminLTE/dist/js/adminlte.min.js',
        'AdminLTE/dist/js/demo.js',
        'js/chart.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}