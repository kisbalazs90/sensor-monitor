<?php

use yii\db\Migration;

/**
 * Class m200625_200449_create_table_light_measurement
 */
class m200625_200449_create_table_light_measurement extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('light_measurement', [
            'id' => $this->primaryKey(),
            'device_id' => $this->integer(),
        ], 'Engine=InnoDB');

        $this->addColumn('light_measurement', 'data1', $this->string(10));
        $this->addColumn('light_measurement', 'data2', $this->string(10));
        $this->addColumn('light_measurement', 'data3', $this->string(10));
        $this->addColumn('light_measurement', 'data4', $this->string(10));
        $this->addColumn('light_measurement', 'data5', $this->string(10));
        $this->addColumn('light_measurement', 'data6', $this->string(10));
        $this->addColumn('light_measurement', 'data7', $this->string(10));
        $this->addColumn('light_measurement', 'created_at', $this->timestamp());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('light_measurement');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200625_200449_create_table_light_measurement cannot be reverted.\n";

        return false;
    }
    */
}
