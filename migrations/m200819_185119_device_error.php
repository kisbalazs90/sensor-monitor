<?php

use yii\db\Migration;

/**
 * Class m200819_185119_device_error
 */
class m200819_185119_device_error extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if (Yii::$app->db->getTableSchema('device_error', true) === null) {
            $this->createTable('device_error', [
            'id' => $this->primaryKey(),
            'serial' => $this->string(),
            'data1' => $this->string(),
            'data2' => $this->string(),
            'data3' => $this->string(),
            'data4' => $this->string(),
            'data5' => $this->string(),
            'created_at' => $this->timestamp()
         ]);
        }
        
        return true;
        
    }
    

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200819_185119_device_error cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200819_185119_device_error cannot be reverted.\n";

        return false;
    }
    */
}
