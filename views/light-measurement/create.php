<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LightMeasurement */

$this->title = 'Create Light Measurement';
$this->params['breadcrumbs'][] = ['label' => 'Light Measurements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="light-measurement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
