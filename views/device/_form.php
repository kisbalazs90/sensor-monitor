<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Device */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="device-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'place_id')->widget(Select2::classname(), [
        'data' => $placeSelect2Data,
        'language' => 'de',
        'options' => ['placeholder' => 'Válassz mérőhelyet'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'serial')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'type')->radioList(['LIGHT' => 'LIGHT', 'TEMP' => 'TEMP']) ?>
    
    <div class="form-group">
        <?= Html::submitButton('Ment', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
