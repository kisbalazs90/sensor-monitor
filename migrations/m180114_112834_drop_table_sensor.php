<?php

use yii\db\Migration;

/**
 * Class m180114_112834_drop_table_sensor
 */
class m180114_112834_drop_table_sensor extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropTable('device');
        $this->renameTable('sensor', 'device');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180114_112834_drop_table_sensor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180114_112834_drop_table_sensor cannot be reverted.\n";

        return false;
    }
    */
}
