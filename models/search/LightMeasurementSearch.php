<?php

namespace app\models\search;

use app\models\LightMeasurement;
use app\models\SensorData;
use Yii;
use DatePeriod;
use DateTime;
use DateInterval;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MeasurementSearch represents the model behind the search form of `app\models\Measurement`.
 */
class LightMeasurementSearch extends LightMeasurement
{

    const FIVE_MINUTE_IN_SEC = 300;

    public $created_at_start;
    public $created_at_end;
    public $created_at;
    public $type_of_stat;
    public $sensor_param;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['device_id', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7',
                'created_at', 'created_at_start', 'created_at_end', 'created_at', 'type_of_stat'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LightMeasurement::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        if (!empty($this->device_id) && is_array($this->device_id)) {
            $query->andWhere(['IN', 'device_id', $this->device_id]);
        }

        if (!empty($params['LightMeasurementSearch']['created_at_start']) && !empty($params['LightMeasurementSearch']['created_at_start'])) {

            $query->andWhere([
                'between',
                'created_at',
                $params['LightMeasurementSearch']['created_at_start'],
                $params['LightMeasurementSearch']['created_at_end']
            ]);
        }

        $query->andFilterWhere(['like', 'data1', $this->data1])
            ->andFilterWhere(['like', 'data2', $this->data2])
            ->andFilterWhere(['like', 'data3', $this->data3])
            ->andFilterWhere(['like', 'data4', $this->data4])
            ->andFilterWhere(['like', 'data5', $this->data5])
            ->andFilterWhere(['like', 'data6', $this->data6])
            ->andFilterWhere(['like', 'data7', $this->data7]);

        $query->orderBy(['id' => SORT_DESC]);

        return $dataProvider;
    }

    /**
     * Ha a requestedData2 üres, akkor csak egy érték fog megjeleníteni a chartokon
     * @param $params
     * @param $requestedData1
     * @param bool $requestedData2
     * @return string
     */
    public function searchForCharts($params)
    {
        $this->load($params);
        $dataNameOfPar = 'data2';
        if (empty($params['LightMeasurementSearch']['sensor_param']) || empty($this->device_id)) {
            return [
                'bottomLabels' => '',
                'meassurementData' => [],
                'devices' => [],
                'sensorData' => [],
                'sumOfPar'=> 0,
            ];
        }

        $query = LightMeasurement::find();
        $query->where(['device_id' => $this->device_id]);
        $query->andWhere([ 'between', 'created_at', $this->created_at_start, $this->created_at_end ]);
        $query->orderBy('created_at', 'ASC');

        $sensorData = SensorData::findAll($params['LightMeasurementSearch']['sensor_param']);

        $someLines = [];
        $dataCounter = 1;
        $labels = '';
        $sumOfPar = 0;
        $lastDate = 0;

        $result = $query->all();

        foreach ($sensorData as $data) {
            $sensorName = $data->name;
            $sensorDataName = $data->json_name;
            $selecetedSensorData = [];
            $collectedSumOfPar = [];
            foreach($result as $row) {
                $selecetedSensorData[] = $row->$sensorDataName;
                if($dataCounter == 1) {
                        $labels .= $row->created_at . ",";
                        $lastPar = $row->data2;
                        $lastDate = $lastDate == 0 ? $row->created_at : $lastDate;
                        $start = new DateTime($lastDate);
                        $lastDate = $row->created_at;
                        $end = new DateTime($row->created_at);
                        $elapsedSecondsBetweenDates = $end->getTimestamp() - $start->getTimestamp();
                        $sumOfPar = $sumOfPar+($elapsedSecondsBetweenDates*$lastPar);
                        $collectedSumOfPar[] = $sumOfPar/1000000;
                    }
            }

            $someLines[$dataCounter] = [
                "label" => "device" . $dataCounter,
                "fillColor" => Yii::$app->params['deviceColors'][$dataCounter],
                "strokeColor" => Yii::$app->params['deviceColors'][$dataCounter],
                "pointColor" => Yii::$app->params['deviceColors'][$dataCounter],
                "pointStrokeColor" => Yii::$app->params['deviceColors'][$dataCounter],
                "pointHighlightFill" => Yii::$app->params['deviceColors'][$dataCounter],
                "pointHighlightStroke" => Yii::$app->params['deviceColors'][$dataCounter],
                "data" => $selecetedSensorData
            ];

            $dataCounter++;

            if ($sensorDataName == $dataNameOfPar) {
                $someLines[$dataCounter] = [
                    "label" => "device" . $dataCounter,
                    "fillColor" => Yii::$app->params['deviceColors'][$dataCounter],
                    "strokeColor" => Yii::$app->params['deviceColors'][$dataCounter],
                    "pointColor" => Yii::$app->params['deviceColors'][$dataCounter],
                    "pointStrokeColor" => Yii::$app->params['deviceColors'][$dataCounter],
                    "pointHighlightFill" => Yii::$app->params['deviceColors'][$dataCounter],
                    "pointHighlightStroke" => Yii::$app->params['deviceColors'][$dataCounter],
                    "data" => $collectedSumOfPar
                ];

                $dataCounter++;
            }
        }

        return [
            'bottomLabels' => $labels,
            'meassurementData' => $someLines,
            'devices' => [$this->device->id],
            'sensorData' => $sensorData,
            'sumOfPar'=> $sumOfPar,
        ];
    }

     /**
     * Ha a requestedData2 üres, akkor csak egy érték fog megjeleníteni a chartokon
     * @param $params
     * @param $requestedData1
     * @param bool $requestedData2
     * @return string
     */
    public function searchForDliCharts($params)
    {
        $this->load($params);

        if (empty($this->device_id)) {
            return [
                'bottomLabels' => '',
                'meassurementData' => [],
                'devices' => [],
                'sensorData' => [],
                'sumOfPar'=> 0,
            ];
        }

        $counter = 0;
        $someLines = [];
        $dataCounter = 1;
        $labels = '';
        $sumOfPar = 0;
        $DAY_TIME_START = ' 00:00:00';
        $DAY_TIME_END = ' 23:59:59';

        $eachDastes = $this->getDatesBetween($this->created_at_start, $this->created_at_end);

        foreach($eachDastes as $ymdDate) {
            $labels .= $ymdDate.', ';
            $currentDateResult = LightMeasurement::find()
            ->select(['data2', 'created_at'])
            ->where([ 'between', 'created_at', $ymdDate.$DAY_TIME_START, $ymdDate.$DAY_TIME_END ])
            ->all();

            $sumOfPar = 0;
            $lastDate = $ymdDate.$DAY_TIME_START;
            foreach($currentDateResult as $lineFromLight) {
                $start = new DateTime($lastDate);
                $end = new DateTime($lineFromLight->created_at);
                $elapsedSecondsBetweenDates = $end->getTimestamp() - $start->getTimestamp();
                $lastDate = $lineFromLight->created_at;
                $lastPar = $lineFromLight->data2;
                $sumOfPar = $sumOfPar+($elapsedSecondsBetweenDates*$lastPar);
            }
            $someLines[]= number_format($sumOfPar/1000000, 2, '.', '');
        }

        return [
            'bottomLabels' => $labels,
            'meassurementData' => [[
                "label" => "device" . $dataCounter,
                "fillColor" => Yii::$app->params['deviceColors'][$dataCounter],
                "strokeColor" => Yii::$app->params['deviceColors'][$dataCounter],
                "pointColor" => Yii::$app->params['deviceColors'][$dataCounter],
                "pointStrokeColor" => Yii::$app->params['deviceColors'][$dataCounter],
                "pointHighlightFill" => Yii::$app->params['deviceColors'][$dataCounter],
                "pointHighlightStroke" => Yii::$app->params['deviceColors'][$dataCounter],
                "data" => $someLines
            ]],
            'devices' => [ $this->device->id ],
            'sensorData' => SensorData::findAll('7'),
            'sumOfPar'=> $sumOfPar,
        ];
    }

        /**
     * Ha a requestedData2 üres, akkor csak egy érték fog megjeleníteni a chartokon
     * @param $params
     * @param $requestedData1
     * @param bool $requestedData2
     * @return string
     */
    public function searchForDliDailyCharts($params)
    {
        $this->load($params);
        $dataNameOfPar = 'data2';
        if (empty($this->device_id)) {
            return [
                'bottomLabels' => '',
                'meassurementData' => [],
                'devices' => [],
                'sensorData' => [],
                'sumOfPar'=> 0,
            ];
        }

        $query = LightMeasurement::find();
        $query->select(['id', 'device_id', 'created_at', $dataNameOfPar]);
        $query->where(['device_id' => $this->device_id]);
        $query->andWhere([ 'between', 'created_at', $this->created_at.' 00:00:00', $this->created_at.' 23:59:59' ]);
        $query->orderBy('created_at', 'ASC');

        $someLines = [];
        $dataCounter = 1;
        $labels = '';
        $sumOfPar = 0;
        $lastDate = 0;
        $count = $query->count();

        $result = $query->all();

            $selecetedSensorData = [];
            $collectedSumOfPar = [];
            foreach($result as $row) {
                $selecetedSensorData[] = 'DLI';
                $labels .= $row->created_at . ",";
                $lastPar = $row->data2;
                $lastDate = $lastDate == 0 ? $row->created_at : $lastDate;
                $start = new DateTime($lastDate);
                $lastDate = $row->created_at;
                $end = new DateTime($row->created_at);
                $elapsedSecondsBetweenDates = $end->getTimestamp() - $start->getTimestamp();
                $sumOfPar = $sumOfPar+($elapsedSecondsBetweenDates*$lastPar);
                $collectedSumOfPar[] = $sumOfPar/1000000;

            }

            $someLines[$dataCounter] = [
                "label" => "device" . $dataCounter,
                "fillColor" => Yii::$app->params['deviceColors'][$dataCounter],
                "strokeColor" => Yii::$app->params['deviceColors'][$dataCounter],
                "pointColor" => Yii::$app->params['deviceColors'][$dataCounter],
                "pointStrokeColor" => Yii::$app->params['deviceColors'][$dataCounter],
                "pointHighlightFill" => Yii::$app->params['deviceColors'][$dataCounter],
                "pointHighlightStroke" => Yii::$app->params['deviceColors'][$dataCounter],
                "data" => $collectedSumOfPar
            ];

        return [
            'bottomLabels' => $labels,
            'meassurementData' => $someLines,
            'devices' => [$this->device->id],
            'sumOfPar'=> $sumOfPar,
        ];
    }

    public function getGridViewColumns($params)
    {
        $columnDetails = [];
        $sensorData = SensorData::find()->where(['type' => SensorData::ENUM_LIGHT])->all();

        foreach ($sensorData as $data) {
            $columnDetails[$data->json_name]['label'] = $data->name;
            $columnDetails[$data->json_name]['visible'] = !$data->is_disabled;
            $columnDetails[$data->json_name]['unit'] = $data->unit;

            if (!empty($params['LightMeasurementSearch']) && !empty($params['LightMeasurementSearch']['sensor_param'])) {
                $visibleSensors = SensorData::findAll($params['LightMeasurementSearch']['sensor_param']);

                if ($visibleSensors) {
                    foreach ($visibleSensors as $visibleSensor) {
                        if ($data->id != $visibleSensor->id) {
                            continue;
                        }

                        $columnDetails[$data->json_name]['visible'] = true;

                    }
                }
            }
             }



        $gridVievColumns = [
            ['class' => 'yii\grid\SerialColumn'],
            'deice' => [
                'header' => 'Eszköz',
                'value' => function ($model) {
                    return !empty($model->device) ? $model->device->name : 'Névtelen eszköz';
                }
            ],
            'data1' => [
                'attribute' => 'data1',
                'filter' => false,
                'value' => function ($model) use ($columnDetails) {
                    return $model->data1 . ' ' . $columnDetails['data1']['unit'];
                },
                'label' => $columnDetails['data1']['label'],
                'visible' => $columnDetails['data1']['visible'],
            ],
            'data2' => [
                'attribute' => 'data2',
                'filter' => false,
                'value' => function ($model) use ($columnDetails) {
                    return $model->data2 . ' ' . $columnDetails['data2']['unit'];
                },
                'label' => $columnDetails['data2']['label'],
                'visible' => $columnDetails['data2']['visible'],
            ],
            'data3' => [
                'attribute' => 'data3',
                'filter' => false,
                'value' => function ($model) use ($columnDetails) {
                    return $model->data3 . ' ' . $columnDetails['data3']['unit'];
                },
                'label' => $columnDetails['data3']['label'],
                'visible' => $columnDetails['data3']['visible'],
            ],
            'data4' => [
                'attribute' => 'data4',
                'filter' => false,
                'value' => function ($model) use ($columnDetails) {
                    return $model->data4 . ' ' . $columnDetails['data4']['unit'];
                },
                'label' => $columnDetails['data4']['label'],
                'visible' => $columnDetails['data4']['visible'],
            ],
            'data5' => [
                'attribute' => 'data5',
                'filter' => false,
                'value' => function ($model) use ($columnDetails) {
                    return $model->data5 . ' ' . $columnDetails['data5']['unit'];
                },
                'label' => $columnDetails['data5']['label'],
                'visible' => $columnDetails['data5']['visible'],
            ],
            'data6' => [
                'attribute' => 'data6',
                'filter' => false,
                'value' => function ($model) use ($columnDetails) {
                    return $model->data6 . ' ' . $columnDetails['data6']['unit'];
                },
                'label' => $columnDetails['data6']['label'],
                'visible' => $columnDetails['data6']['visible'],
            ],
            'data7' => [
                'attribute' => 'data7',
                'filter' => false,
                'value' => function ($model) use ($columnDetails) {
                    return $model->data7 . ' ' . $columnDetails['data7']['unit'];
                },
                'label' => $columnDetails['data7']['label'],
                'visible' => $columnDetails['data7']['visible'],
            ],
            'created_at' => [
                'attribute' => 'created_at',
                'filter' => false,
                'value' => function ($model) {
                    return $model->created_at;
                }
            ],
        ];

        return $gridVievColumns;
    }

    private function getDatesBetween($date1, $date2) {
        $array = [];
        $Variable1 = strtotime($date1);
        $Variable2 = strtotime($date2);
        // Use for loop to store dates into array
        // 86400 sec = 24 hrs = 60*60*24 = 1 day
        for ($currentDate = $Variable1; $currentDate <= $Variable2; $currentDate += (86400)) {
            $Store = date('Y-m-d', $currentDate);
            $array[] = $Store;
        }

        return $array;
    }

}
