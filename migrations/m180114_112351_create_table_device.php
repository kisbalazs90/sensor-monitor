<?php

use yii\db\Migration;

/**
 * Class m180114_112351_create_table_device
 */
class m180114_112351_create_table_device extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('device', [
            'id' => $this->primaryKey(),
            'place_id' => $this->integer(),
            'name' => $this->string(255),
            'serial' => $this->string(),
            'created_at' => $this->timestamp()
        ], 'Engine=InnoDB');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180114_112351_create_table_device cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180114_112351_create_table_device cannot be reverted.\n";

        return false;
    }
    */
}
