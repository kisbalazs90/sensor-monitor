<?php

use yii\db\Migration;

/**
 * Class m180114_150340_reset_database
 */
class m180114_150340_reset_database extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->truncateTable('device');
        $this->truncateTable('sensor_data');
        $this->truncateTable('measurement');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
        $this->insert('device', [
            'place_id' => 1,
            'name' => 'FS1-Device',
        ]);

        $this->execute('
        INSERT INTO `sensor_data` (`id`, `device_id`, `name`, `json_name`, `unit`, `created_at`) 
            VALUES
            (1, 1, \'Bejövő vízhőfok\', \'data1\', \'C°\', \'2018-01-14 14:36:26\'),
            (2, 1, \'Kimenő vízhőfok\', \'data2\', \'C°\', \'2018-01-14 14:36:26\'),
            (3, 1, \'data3\', \'data3\', NULL, \'2018-01-14 14:36:26\'),
            (4, 1, \'data4\', \'data4\', NULL, \'2018-01-14 14:36:26\'),
            (5, 1, \'data5\', \'data5\', NULL, \'2018-01-14 14:36:26\');
        ');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180114_150340_reset_database cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180114_150340_reset_database cannot be reverted.\n";

        return false;
    }
    */
}
