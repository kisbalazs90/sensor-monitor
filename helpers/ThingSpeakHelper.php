<?php

namespace app\helpers;

use Yii;

class ThingspeakHelper {
	function __construct() {
		$this->curl = curl_init();
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->curl, CURLOPT_ACCEPT_ENCODING, 'gzip');
		curl_setopt($this->curl, CURLOPT_ENCODING, 'gzip');

		$this->endpoint = 'https://api.thingspeak.com/';
	}

	function __destruct() {
		curl_close($this->curl);
	}

	public function loadChannel($channelId, $apiKey, $results = 0) {
		$channel = false;

		$response = $this->get('channels/'.$channelId.'/feeds.json?api_key='.$apiKey.'&results='.$results);

		if(!$response->errors) {
			$channel = new \stdClass();
			$channel->id = $channelId;
			$channel->name = $response->response->channel->name;
			$channel->fields = array();
			$channel->data = array();
			$channel->readApiKey = $apiKey;

			for($i = 0; $i < 10; $i++) {
				if(isset($response->response->channel->{'field'.$i})) $channel->fields[$i] = $response->response->channel->{'field'.$i};
			}

			if($results) {
				foreach($response->response->feeds AS $entry) {
					for($i = 0; $i < 10; $i++) {
						if(isset($entry->{'field'.$i})) {
							if(!isset($channel->data[$i])) $channel->data[$i] = array();
							$channel->data[$i][$entry->created_at] = $entry->{'field'.$i};
						}
					}
				}
			}
		}

		return $channel;
	}

	public function loadLastValue($channelId, $apiKey, $fieldId) {
		$response = $this->get('channels/'.$channelId.'/fields/'.$fieldId.'/last.json?api_key='.$apiKey);

		if(!$response->errors) {
			if(isset($response->response->{'field'.$fieldId})) return $response->response->{'field'.$fieldId};
		}

		return '';
	}

	public function writeData($fields, $writeApiKey=false) {
    if ($writeApiKey === false) {
      $writeApiKey = Yii::$app->params['thingSpeak']['write_api_key'];
    }

		$fieldsTxt = array();
		foreach($fields AS $fieldId => $value) {
			$fieldsTxt[] = 'field'.$fieldId.'='.$value;
		}

		$fieldsTxt = implode('&', $fieldsTxt);

		$response = $this->get('update.json?api_key='.$writeApiKey.'&'.$fieldsTxt);
		return $response;
		//if(!$response->errors) {
		//	if(isset($response->response->entry_id)) return $response->response->entry_id;
		//	return true;
		//}

		//return false;
	}

	private function get($url) {
		$start = microtime(true);
		$url = $this->endpoint.$url;

		curl_setopt($this->curl, CURLOPT_URL, $url);
		curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($this->curl, CURLOPT_POST, 0);
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, false);

		$response = curl_exec($this->curl);
	    $errors = curl_error($this->curl);
	    $info = curl_getinfo($this->curl);

    //$output = new \stdClass();
		//$output->response = json_decode($response);
		//$output->errors = $errors;
		//$output->code = $info['http_code'];
		//if($output->code != 200) $output->errors = 'Error code '.$output->code.': '.$output->errors;

		//return $output;
	}
}