<?php

return [
    'adminEmail' => 'admin@example.com',
    'deviceColors' => [
        1 => '#ff0000',
        2 => '#ff7e00',
        3 => '#00d200',
        4 => '#00d296',
        5 => '#0000d2',
        6 => '#9600d2',
        7 => '#D2509C',
        8 => '#D296D2',
        9 => '#F0753A',
        10 => '#D2D200',
    ],
    'thingSpeak' => [
        'write_api_key' => 'L7374DVFZ6R3MPLW'
    ]
];