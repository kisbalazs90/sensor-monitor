<?php

use yii\db\Migration;

/**
 * Class m200628_124326_add_column_to_sensor_data
 */
class m200628_124326_add_column_to_sensor_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $this->dropColumn('sensor_data', 'type');
        $this->addColumn('sensor_data', 'type', "ENUM('TEMP', 'LIGHT')");
        $this->execute("UPDATE sensor_data SET type = 'TEMP'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('sensor_data', 'type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200628_124326_add_column_to_sensor_data cannot be reverted.\n";

        return false;
    }
    */
}
