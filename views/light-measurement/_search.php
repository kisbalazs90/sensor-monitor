<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\LightMeasurementSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="light-measurement-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'device_id') ?>

    <?= $form->field($model, 'data1') ?>

    <?= $form->field($model, 'data2') ?>

    <?= $form->field($model, 'data3') ?>

    <?php // echo $form->field($model, 'data4') ?>

    <?php // echo $form->field($model, 'data5') ?>

    <?php // echo $form->field($model, 'data6') ?>

    <?php // echo $form->field($model, 'data7') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
