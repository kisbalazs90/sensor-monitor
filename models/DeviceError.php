<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "device_error".
 *
 * @property int $id
 * @property string $serial
 * @property string $data1
 * @property string $data2
 * @property string $data3
 * @property string $data4
 * @property string $data5
 * @property string $created_at
 */
class DeviceError extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_error';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['serial'], 'required'],
            [['created_at'], 'safe'],
            [['serial', 'data5'], 'string', 'max' => 100],
            [['data1', 'data2', 'data3', 'data4'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'serial' => 'SOROZATSZÁM',
            'data1' => 'ERROR KÓDOK',
            'data2' => 'ÁLLAPOT KÓDOK',
            'data3' => 'ESP8266 TÁPFESZÜLTSÉG',
            'data4' => 'WI-FI JELERŐSSÉG',
            'data5' => 'FUTÁSIDŐ ÓRÁBAN',
            'created_at' => 'LÉTREHOZVA',
        ];
    }
}
