<?php

use yii\db\Migration;

/**
 * Class m180114_113134_alter_table_sensor_data
 */
class m180114_113134_alter_table_sensor_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('sensor_data', 'sensor_id', 'device_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180114_113134_alter_table_sensor_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180114_113134_alter_table_sensor_data cannot be reverted.\n";

        return false;
    }
    */
}
