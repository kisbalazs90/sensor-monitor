<?php

use yii\db\Migration;

/**
 * Class m180114_130626_alter_table
 */
class m180114_130626_alter_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('measurement', 'device_id', $this->integer());
        $this->addForeignKey('measurement_device_id', 'measurement', 'device_id', 'device', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180114_130626_alter_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180114_130626_alter_table cannot be reverted.\n";

        return false;
    }
    */
}
