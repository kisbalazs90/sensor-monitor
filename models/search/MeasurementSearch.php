<?php

namespace app\models\search;

use app\models\Measurement;
use app\models\SensorData;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MeasurementSearch represents the model behind the search form of `app\models\Measurement`.
 */
class MeasurementSearch extends Measurement
{

    const FIVE_MINUTE_IN_SEC = 300;

    public $created_at_start;
    public $created_at_end;
    public $created_at;
    public $type_of_stat;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['device_id', 'data1', 'data2', 'data3', 'data4', 'data5',
                'created_at', 'created_at_start', 'created_at_end', 'created_at', 'type_of_stat'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Measurement::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        if (!empty($this->device_id) && is_array($this->device_id)) {
            $query->andWhere(['IN', 'device_id', $this->device_id]);
        }

        if (!empty($params['MeasurementSearch']['created_at_start']) && !empty($params['MeasurementSearch']['created_at_start'])) {

            $query->andWhere([
                'between',
                'created_at',
                $params['MeasurementSearch']['created_at_start'],
                $params['MeasurementSearch']['created_at_end']
            ]);
        }

        $query->andFilterWhere(['like', 'data1', $this->data1])
            ->andFilterWhere(['like', 'data2', $this->data2])
            ->andFilterWhere(['like', 'data3', $this->data3])
            ->andFilterWhere(['like', 'data4', $this->data4])
            ->andFilterWhere(['like', 'data5', $this->data5]);

        $query->orderBy(['id' => SORT_DESC]);

        return $dataProvider;
    }

    /**
     * Ha a requestedData2 üres, akkor csak egy érték fog megjeleníteni a chartokon
     * @param $params
     * @param $requestedData1
     * @param bool $requestedData2
     * @return string
     */
    public function searchForCharts($params, $requestedData1, $requestedData2 = false)
    {
        $this->load($params);

        if (empty($this->device_id) || !is_array($this->device_id)) {
            return;
        }

        if ($requestedData2) {
            return $this->withTwoDataSet($requestedData1, $requestedData2);
        } else {
            return $this->withOneDataSet($requestedData1, $requestedData2);
        }
    }

    /**
     * @param $requestedData1
     * @param $requestedData2
     * @return array
     */
    private function withOneDataSet($requestedData1, $requestedData2)
    {
        $collectedMeassurements = $this->collectMeassurements();
        $syncronizedContent = $this->syncronizeMeassurements($collectedMeassurements, $requestedData1, $requestedData2);
        if (empty($syncronizedContent) || !$syncronizedContent['labels'] || !$syncronizedContent['measurement']) {
            return [
                'bottomLabels' => null,
                'meassurementData' => null,
                'devices' => null,
            ];
        }

        $transformedResult = [];
        $labels = '';
        $devices = [];
        foreach ($syncronizedContent['labels'] as $label) {
            $labels .= $label . ",";
        }

        $measurement = $syncronizedContent['measurement'];

        $i = 0;
        foreach ($measurement as $device => $measurementData) {
            $i++;
            $transformedResult[$i] = $this->getChartSchema($i);
            foreach ($measurementData['data1'] as $value) {
                $transformedResult[$i]['data'][] = floatval($value);
            }

            $devices[] = $device;
        }

        return [
            'bottomLabels' => $labels,
            'meassurementData' => $transformedResult,
            'devices' => $devices,
        ];
    }

    /**
     * @param $requestedData1
     * @param $requestedData2
     * @return array
     */
    private function withTwoDataSet($requestedData1, $requestedData2)
    {
        $collectedMeassurements = $this->collectMeassurements();
        $syncronizedContent = $this->syncronizeMeassurements($collectedMeassurements, $requestedData1, $requestedData2);

        if (empty($syncronizedContent) || !$syncronizedContent['labels'] || !$syncronizedContent['measurement']) {
            return;
        }

        $transformedResult = [];
        $labels = '';
        $devices = [];
        foreach ($syncronizedContent['labels'] as $label) {
            $labels .= $label . ",";
        }

        $measurement = $syncronizedContent['measurement'];

        $i = 0;
        foreach ($measurement as $device => $measurementData) {
            $i++;
            $transformedResult[$i] = $this->getChartSchema($i);
            foreach ($measurementData['data1'] as $value) {
                $transformedResult[$i]['data'][] = floatval($value);
            }

            $i++;
            $transformedResult[$i] = $this->getChartSchema($i);
            foreach ($measurementData['data2'] as $value) {
                $transformedResult[$i]['data'][] = floatval($value);
            }
            $devices[] = $device;
        }

        return [
            'bottomLabels' => $labels,
            'meassurementData' => $transformedResult,
            'devices' => $devices
        ];
    }


    /**
     * @param $i
     * @return array
     */
    private function getChartSchema($i)
    {
        return [
            "label" => "device" . $i,
            "fillColor" => Yii::$app->params['deviceColors'][$i],
            "strokeColor" => Yii::$app->params['deviceColors'][$i],
            "pointColor" => Yii::$app->params['deviceColors'][$i],
            "pointStrokeColor" => Yii::$app->params['deviceColors'][$i],
            "pointHighlightFill" => Yii::$app->params['deviceColors'][$i],
            "pointHighlightStroke" => Yii::$app->params['deviceColors'][$i],
        ];
    }

    /**
     * @return array
     */
    private function collectMeassurements()
    {
        if (!empty($this->created_at)) {
            $this->created_at_start = date('Y-m-d H:i:s', strtotime($this->created_at) - self::FIVE_MINUTE_IN_SEC);
            $this->created_at_end = date('Y-m-d H:i:s', strtotime($this->created_at) + self::FIVE_MINUTE_IN_SEC);
        }

        $collectedMeassurements = [];

        if (empty($this->created_at) && empty($this->created_at_start)) {
            $this->created_at_start = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')) - '2 hours');
        }
        if (empty($this->created_at) && empty($this->created_at_end)) {
            $this->created_at_end = date('Y-m-d H:i:s');
        }

        for ($i = 0; $i < sizeof($this->device_id); $i++) {
            $query = Measurement::find();
            $query->where(['device_id' => $this->device_id[$i]]);
            $query->andWhere([
                    'between',
                    'created_at',
                    $this->created_at_start,
                    $this->created_at_end
                ]);

            //$queryCommandInReadableFormat = $query->createCommand()->getRawSql();
            $collectedMeassurements[$this->device_id[$i]] = $query->asArray()->all();
        }

        return $collectedMeassurements;


    }

    /**
     * @param $measurements
     * @param $requestedData1
     * @param $requestedData2
     * @return array
     */
    private function syncronizeMeassurements($measurements, $requestedData1, $requestedData2 = false)
    {
        $countOfBiggestArray = 0;
        $referenceDevice = 0;
        $biggestMeasurementsArray = [];
        $plusMinusMax = self::FIVE_MINUTE_IN_SEC;

        foreach ($measurements as $device => $measureData) {
            if ($countOfBiggestArray <= sizeof($measureData)) {
                $countOfBiggestArray = sizeof($measureData);
                $referenceDevice = $device;
                $biggestMeasurementsArray = $measureData;
            }
        }

        $arrayWalkSteps = floor($countOfBiggestArray / 80);

        $synchronisedData = [];

        for ($i = 0; $i < sizeof($biggestMeasurementsArray); $i++) {

            if ($arrayWalkSteps != 0 && $i % $arrayWalkSteps !== 0) {
                continue;
            }

            $referenceDeviceData = $biggestMeasurementsArray[$i];
            $synchronisedData['labels'][] = $referenceDeviceData['created_at'];
            $synchronisedData['measurement'][$referenceDevice]['data1'][] = $referenceDeviceData[$requestedData1];
            if ($requestedData2)
                $synchronisedData['measurement'][$referenceDevice]['data2'][] = $referenceDeviceData[$requestedData2];


            foreach ($measurements as $deviceId => $measureData) {
                if ($deviceId == $referenceDevice) {
                    continue;
                }

                $theSyncedData = Measurement::find()
                    ->where(['device_id' => $deviceId])
                    ->andWhere([
                        'between',
                        'created_at',
                        date('Y-m-d H:i:s', strtotime($referenceDeviceData['created_at']) - $plusMinusMax),
                        date('Y-m-d H:i:s', strtotime($referenceDeviceData['created_at']) + $plusMinusMax)
                    ])->one();

                if ($theSyncedData) {
                    $synchronisedData['measurement'][$deviceId]['data1'][] = $theSyncedData->$requestedData1;
                    if ($requestedData2)
                        $synchronisedData['measurement'][$deviceId]['data2'][] = $theSyncedData->$requestedData2;
                } else {
                    $synchronisedData['measurement'][$deviceId]['data1'][] = '0';
                    if ($requestedData2)
                        $synchronisedData['measurement'][$deviceId]['data2'][] = '0';
                }

            }
        }

        return $synchronisedData;
    }

    public function getGridViewColumns()
    {
        $columnDetails = [];

        $sensorData = SensorData::find()->where(['type' => 'TEMP'])->all();

        foreach ($sensorData as $data) {
            $columnDetails[$data->json_name]['label'] = $data->name;
            $columnDetails[$data->json_name]['visible'] = $data->is_disabled ? false : true;
            $columnDetails[$data->json_name]['unit'] = $data->unit;
        }

        $gridVievColumns = [
            ['class' => 'yii\grid\SerialColumn'],
            'deice' => [
                'header' => 'Eszköz',
                'value' => function ($model) {
                    return !empty($model->device) ? $model->device->name : 'Névtelen eszköz';
                }
            ],
            'data1' => [
                'attribute' => 'data1',
                'filter' => false,
                'value' => function ($model) use ($columnDetails) {
                    return $model->data1 . ' ' . $columnDetails['data1']['unit'];
                },
                'label' => $columnDetails['data1']['label'],
                'visible' => $columnDetails['data1']['visible'],
            ],
            'data2' => [
                'attribute' => 'data2',
                'filter' => false,
                'value' => function ($model) use ($columnDetails) {
                    return $model->data2 . ' ' . $columnDetails['data2']['unit'];
                },
                'label' => $columnDetails['data2']['label'],
                'visible' => $columnDetails['data2']['visible'],
            ],
            'data3' => [
                'attribute' => 'data3',
                'filter' => false,
                'value' => function ($model) use ($columnDetails) {
                    return $model->data3 . ' ' . $columnDetails['data3']['unit'];
                },
                'label' => $columnDetails['data3']['label'],
                'visible' => $columnDetails['data3']['visible'],
            ],
            'data4' => [
                'attribute' => 'data4',
                'filter' => false,
                'value' => function ($model) use ($columnDetails) {
                    return $model->data4 . ' ' . $columnDetails['data4']['unit'];
                },
                'label' => $columnDetails['data4']['label'],
                'visible' => $columnDetails['data4']['visible'],
            ],
            'data5' => [
                'attribute' => 'data5',
                'filter' => false,
                'value' => function ($model) use ($columnDetails) {
                    return $model->data5 . ' ' . $columnDetails['data5']['unit'];
                },
                'label' => $columnDetails['data5']['label'],
                'visible' => $columnDetails['data5']['visible'],
            ],
            'created_at' => [
                'attribute' => 'created_at',
                'filter' => false,
                'value' => function ($model) {
                    return $model->created_at;
                }
            ],
        ];

        return $gridVievColumns;
    }

}
