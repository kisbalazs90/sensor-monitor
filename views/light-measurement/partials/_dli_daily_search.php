<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\MeasurementSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .s2-togall-button {
        display: none !important;
    }
</style>

<div class="measurement-search">

    <?php
    $form = ActiveForm::begin([
        'action' => [$action],
        'method' => 'get',
    ]);
    ?>

    <?php
    $model = new app\models\search\LightMeasurementSearch();
    ?>

    <div class="row">
        <div class="col-md-2">
            <label for="device_id">Eszköz kiválasztása</label>
            <?=
            $form->field($model, 'device_id')->widget('\kartik\select2\Select2', [
                'data' => $deviceSelect2List,
                'options' => [
                    'showToggleAll' => false,
                    'placeholder' => 'Válassz...',
                    'multiple' => false,
                    'value' => 61,
                ],
                'pluginOptions' => [
                    'maximumSelectionLength'=> 5,
                ],
            ])->label(false)
            ?>
        </div>
        <div class="col-md-2">
        <label for="created_at_start">Dátum</label>
        <?=
        $form->field($model, 'created_at')->widget('\kartik\date\DatePicker', [
                'name' => 'created_at',
                'type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND,
                'value' => date('Y-m-d'),
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]
        )->label(false)
        ?>
        </div>

        <div class="col-md-1 no-padding">
            <label for="">&nbsp;</label>
            <div class="form-group">
                <?= Html::submitButton('Keresés', ['class' => 'btn btn-primary']) ?>
            </div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
