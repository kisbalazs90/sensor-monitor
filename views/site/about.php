<?php

/* @var $this yii\web\View */

$this->title = 'Sensor Monitor';
\app\assets\ChartAsset::register($this);
?>

<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Névjegy</h3>
                    </div>
                    <div class="box-body">
                        <p><b>Program Név:</b> Hőmérés adatgyűjtés</p>
                        <p><b>Végfelhasználó:</b> Szent Lászlói Fóliás Kertészek Mg. Szolg. Szöv.</p>

                        <p><b>Fejlesztői kapcsolat:</b> kisbalazs90@gmail.com</p>
                        <p><b>Verzió:</b> 2.2.2</p>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
