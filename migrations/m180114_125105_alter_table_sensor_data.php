<?php

use yii\db\Migration;

/**
 * Class m180114_125105_alter_table_sensor_data
 */
class m180114_125105_alter_table_sensor_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->execute('ALTER TABLE `sensor_data` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180114_125105_alter_table_sensor_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180114_125105_alter_table_sensor_data cannot be reverted.\n";

        return false;
    }
    */
}
