<?php

use yii\db\Migration;

/**
 * Class m180114_125454_alter_table_sensor_data
 */
class m180114_125454_alter_table_sensor_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('sensor_data', 'name', $this->string(255));
        $this->alterColumn('sensor_data', 'unit', $this->string(2));
        $this->alterColumn('sensor_data', 'direction', $this->smallInteger(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180114_125454_alter_table_sensor_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180114_125454_alter_table_sensor_data cannot be reverted.\n";

        return false;
    }
    */
}
