<?php

namespace app\controllers;

use app\models\Device;
use app\models\DeviceError;
use app\models\Measurement;
use app\models\LightMeasurement;
use yii\helpers\Json;
use app\helpers\ThingspeakHelper;

class ApiController extends \yii\rest\Controller
{
    const SUCCESS_INSERT = 1;
    const ERROR_WITH_SERIAL = 2;
    const ERROR_DURING_SAVE = 3;
    const MISSING_JSON_DATA = 4;
    const OUT_OF_RANGE = 5;

    /*
     * API call
     */
    public function actionInsert()
    {
        $jsonData = Json::decode(file_get_contents('php://input'), false);

        if (empty($jsonData)) {
            return self::MISSING_JSON_DATA;
        }

        $device = Device::findOne(['serial' => $jsonData->serial]);

        if (empty($device)) {
            return self::ERROR_WITH_SERIAL;
        }

        return $this->insertMeasurent($device, $jsonData);
    }


    /*
     * API call
     */
    public function actionErrorInsert()
    {
        $jsonData = Json::decode(file_get_contents('php://input'), false);

        if (empty($jsonData)) {
            return self::MISSING_JSON_DATA;
        }

        return $this->insertError($jsonData);
    }

    /*
     * API call
     * SAMPLE
     {
        "serial": "0154",
        "data1": "234",
        "data2": "543",
     }
     */
    public function actionLightMeasurementInsert()
    {
        $jsonData = Json::decode(file_get_contents('php://input'), false);

        $device = Device::findOne(['serial' => $jsonData->serial]);

        if (empty($device)) {
            return self::ERROR_WITH_SERIAL;
        }

        if (empty($jsonData)) {
            return self::MISSING_JSON_DATA;
        }

        return $this->insertLightMeasurent($device, $jsonData);
    }

    /*
     * API call
     * SAMPLE
     {
        "serial": "0154",
        "data1": "234",
        "data2": "543",
     }
    */
    public function actionGetLightMeasurementData()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $serialCode = \Yii::$app->request->get('serial');

        if (!$serialCode) {
            return ['error' => 'Serial is a required parameter'];
        }

        if (!$deviceBySerial = Device::findOne(['serial' => $serialCode])) {
            return ['error' => 'Invalid serial'];
        }

        $days = !empty(\Yii::$app->request->get('days')) ? \Yii::$app->request->get('days')-1: 0;

        if ($days > 9) {
            $days = 9;
        }

        $today = date('y-m-d');
        $minusDays = '-'.$days.' days';
        $countedDate = date('Y-m-d', strtotime($minusDays));

        $result = [];
        $counter = 0;
        $dataCounter = 1;
        $labels = '';
        $sumOfPar = 0;
        $DAY_TIME_START = ' 00:00:00';
        $DAY_TIME_END = ' 23:59:59';
        $result = [];
        $eachDastes = $this->getDatesBetween($countedDate, $today);

        foreach($eachDastes as $ymdDate) {

            $labels .= $ymdDate.', ';
            $currentDateResult = LightMeasurement::find()
            ->select(['data2', 'created_at'])
            ->where(['device_id' => $deviceBySerial->id])
            ->andWhere([ 'between', 'created_at', $ymdDate.$DAY_TIME_START, $ymdDate.$DAY_TIME_END ])
            ->all();

            $sumOfPar = 0;
            $lastDate = $ymdDate.$DAY_TIME_START;
            foreach($currentDateResult as $lineFromLight) {
                $start = new \DateTime($lastDate);
                $end = new \DateTime($lineFromLight->created_at);
                $elapsedSecondsBetweenDates = $end->getTimestamp() - $start->getTimestamp();
                $lastDate = $lineFromLight->created_at;
                $lastPar = $lineFromLight->data2;
                $sumOfPar = $sumOfPar+($elapsedSecondsBetweenDates*$lastPar);
            }

            $result[$counter]['par'] = $sumOfPar;
            $result[$counter]['dli'] = number_format($sumOfPar/1000000, 2, '.', '');
            $result[$counter]['current_day'] = $counter;
            $result[$counter]['date'] = $ymdDate;
            $result[$counter]['server_time'] = date('Y-m-d H:i:s');
            $result[$counter]['server_time_tt'] = strtotime(date('Y-m-d H:i:s'));
            $result[$counter]['serial'] = $serialCode;
            $counter++;
        }

        return $result;
    }

    public function actionGetLatestLightMeasurementData()
    {
        $serialCode = \Yii::$app->request->get('serial');
        $data = [];

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($serialCode ) {
            if (!$deviceBySerial = Device::findOne(['serial' => $serialCode])) {
                return ['error' => 'Invalid serial'];
            }
        } else {
            return ['error' => 'Serial is a required parameter'];
        }

         $data = LightMeasurement::find()->where(['device_id' => $deviceBySerial->id])->orderBy("id DESC")->limit(1)->asArray()->all();

         foreach ($data as &$measurement) {
            $measurement['created_at_tt'] = strtotime($measurement['created_at']);
            $measurement['server_time'] = date('Y-m-d H:i:s');
            $measurement['server_time_tt'] = strtotime(date('Y-m-d H:i:s'));
            $measurement['serial'] = $serialCode;
        }

        return $data;
    }



    protected function insertError($jsonData) {
        $deviceErrorModel = new DeviceError();

        if (!empty($jsonData->serial)) {
            $deviceErrorModel->serial = $jsonData->serial;
        }

        if (!empty($jsonData->data1)) {
            $deviceErrorModel->data1 = $jsonData->data1;
        }

        if (!empty($jsonData->data2)) {
            $deviceErrorModel->data2 = $jsonData->data2;
        }

        if (!empty($jsonData->data3)) {
            $deviceErrorModel->data3 = $jsonData->data3;
        }

        if (!empty($jsonData->data4)) {
            $deviceErrorModel->data4 = $jsonData->data4;
        }

        if (!empty($jsonData->data5)) {
            $deviceErrorModel->data5 = $jsonData->data5;
        }

        if (!$deviceErrorModel->save()) {
            return self::ERROR_DURING_SAVE;
        }

        return self::SUCCESS_INSERT;
    }


    /**
     * @param $device
     * @param $jsonData
     * @return int
     */
    protected function insertMeasurent($device, $jsonData)
    {
        $measurementModel = new Measurement();
        $measurementModel->device_id = $device->id;
        $valid = true;

        if (!empty($jsonData->data1)) {
            $valid = $this->validateHeatRange($jsonData->data1);
            $measurementModel->data1 = $jsonData->data1;
        }

        if (!empty($jsonData->data2)) {
            $valid = $this->validateHeatRange($jsonData->data2);
            $measurementModel->data2 = $jsonData->data2;
        }

        if (!empty($jsonData->data3)) {
            $valid = $this->validateHeatRange($jsonData->data3);
            $measurementModel->data3 = $jsonData->data3;
        }

        if (!empty($jsonData->data4)) {
            $valid = $this->validateHeatRange($jsonData->data4);
            $measurementModel->data4 = $jsonData->data4;
        }

        if (!empty($jsonData->data5)) {
            $valid = $this->validateHeatRange($jsonData->data5);
            $measurementModel->data5 = $jsonData->data5;
        }

        if (!$valid) {
            return self::OUT_OF_RANGE;
        }

        if (!$measurementModel->save()) {
            return self::ERROR_DURING_SAVE;
        }

        return self::SUCCESS_INSERT;
    }


    public function validateHeatRange($data)
    {
        $number = (float)$data;
        if ($number < -100 || $number > 84.9) {
            return false;
        }

        return true;
    }

    /**
     * @param $device
     * @param $jsonData
     * @return int
     */
    protected function insertLightMeasurent($device, $jsonData)
    {
        $lightMeasurementModel = new LightMeasurement();
        $lightMeasurementModel->device_id = $device->id;
        $valid = true;

        if (!empty($jsonData->data1)) {
            $lightMeasurementModel->data1 = $jsonData->data1;
        }

        if (!empty($jsonData->data2)) {
            $lightMeasurementModel->data2 = $jsonData->data2;

            $ymdDate = date('Y-m-d');
            $DAY_TIME_START = ' 00:00:00';
            $DAY_TIME_END = ' 23:59:59';
            $currentDateResult = LightMeasurement::find()
            ->select(['data2', 'created_at'])
            ->where([ 'between', 'created_at', $ymdDate.$DAY_TIME_START, $ymdDate.$DAY_TIME_END ])
            ->all();

            $sumOfPar = 0;
            $lastDate = $ymdDate.$DAY_TIME_START;
            foreach($currentDateResult as $lineFromLight) {
                $start = new \DateTime($lastDate);
                $end = new \DateTime($lineFromLight->created_at);
                $elapsedSecondsBetweenDates = $end->getTimestamp() - $start->getTimestamp();
                $lastDate = $lineFromLight->created_at;
                $lastPar = $lineFromLight->data2;
                $sumOfPar = $sumOfPar+($elapsedSecondsBetweenDates*$lastPar);
            }

            $currentDLI = number_format($sumOfPar/1000000, 2, '.', '');

            (new ThingspeakHelper())->writeData([
                1 => $currentDLI, // Ez az összeadott DLI
                2 => $jsonData->data2, // Ez az aktuális PAR
                3 => $jsonData->data3 // Ez az külső hőmérséklet
            ]);
        }

        if (!empty($jsonData->data3)) {
            $lightMeasurementModel->data3 = $jsonData->data3;
        }

        if (!empty($jsonData->data4)) {
            $lightMeasurementModel->data4 = $jsonData->data4;
        }

        if (!empty($jsonData->data5)) {
            $lightMeasurementModel->data5 = $jsonData->data5;
        }

        if (!empty($jsonData->data6)) {
            $lightMeasurementModel->data6 = $jsonData->data6;
        }

        if (!empty($jsonData->data7)) {
            $lightMeasurementModel->data7 = $jsonData->data7;
        }

        if (!$lightMeasurementModel->save()) {
            return self::ERROR_DURING_SAVE;
        }

        return self::SUCCESS_INSERT;
    }

    private function getDatesBetween($date1, $date2) {
        $array = [];
        $Variable1 = strtotime($date1);
        $Variable2 = strtotime($date2);
        // Use for loop to store dates into array
        // 86400 sec = 24 hrs = 60*60*24 = 1 day
        for ($currentDate = $Variable1; $currentDate <= $Variable2; $currentDate += (86400)) {
            $Store = date('Y-m-d', $currentDate);
            $array[] = $Store;
        }

        return $array;
    }

}