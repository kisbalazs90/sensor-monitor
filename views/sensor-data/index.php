<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PlaceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<div class="place-index">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h4><?= Yii::t('app', 'Sensor adatok'); ?></h4>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <?= Html::a('Új', ['create'], ['class' => 'btn btn-success']) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'filterModel' => $searchModel,
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn'],
                                            'id',
                                            'name',
                                            'type',
                                            ['class' => 'yii\grid\ActionColumn'],
                                        ],
                                    ]); ?>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.box -->
</div>
