<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LightMeasurement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="light-measurement-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'device_id')->textInput() ?>

    <?= $form->field($model, 'data1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'data2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'data3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'data4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'data5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'data6')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'data7')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
