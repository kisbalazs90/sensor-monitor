<?php

namespace app\controllers;

use Yii;
use app\models\DeviceError;
use app\models\search\DeviceError as DeviceErrorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DeviceErrorController implements the CRUD actions for DeviceError model.
 */
class DeviceErrorController extends BaseController
{
    /**
     * Lists all DeviceError models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeviceErrorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the DeviceError model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DeviceError the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DeviceError::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
