<?php
use webvimark\modules\UserManagement\components\GhostMenu;
use webvimark\modules\UserManagement\UserManagementModule;
?>

<aside class="main-sidebar">

    <section class="sidebar">
        <?= GhostMenu::widget([
            'encodeLabels' => false,
            'activateParents' => true,
            'options' => [
                    'class' => 'sidebar-menu',
                    'data-widget' => 'tree',
                ],
            'items' => [
                ['label' => '<i class="fa fa-home" aria-hidden="true"></i> Mérőhelyek', 'url' => ['/place/index']],
                ['label' => '<i class="fa fa-microchip" aria-hidden="true"></i> Eszközök', 'url' => ['/device/index']],
                ['label' => '<i class="fa fa-exclamation-triangle " aria-hidden="true"></i> Eszköz hibák', 'url' => ['/device-error/index']],
                ['label' => '<i class="fa fa-rss" aria-hidden="true"></i>Eszközök állapota', 'url' => ['/device/index-read-only']],
                ['label' => '<i class="fa fa-cogs" aria-hidden="true"></i> Sensoradatok', 'url' => ['/sensor-data/index']],
                ['label' => '<i class="fa fa-thermometer-full" aria-hidden="true"></i> Hő és Nyom. mérések', 'url' => ['/measurement/index']],
                ['label' => '<i class="fa fa-thermometer-full" aria-hidden="true"></i>  Hőmérséklet statisztika', 'url' => ['/measurement/meassure-stat']],
                ['label' => '<i class="fa fa-tint" aria-hidden="true"></i>  Nyomás statisztika', 'url' => ['/measurement/pressure-stat']],
                ['label' => '<i class="fa fa-bar-chart" aria-hidden="true"></i> Csoportos statisztika', 'url' => ['/measurement/groups-stat']],
                ['label' => '<i class="fa fa-lightbulb-o" aria-hidden="true"></i> Fény mérések', 'url' => ['/light-measurement/index']],
                ['label' => '<i class="fa fa-lightbulb-o" aria-hidden="true"></i> Fény mérés stat. (PAR)', 'url' => ['/light-measurement/light-stat']],
                ['label' => '<i class="fa fa-lightbulb-o" aria-hidden="true"></i> Fény mérés stat. (DLI) ', 'url' => ['/light-measurement/dli-stat']],
                ['label' => '<i class="fa fa-lightbulb-o" aria-hidden="true"></i> DLI napi statisztika', 'url' => ['/light-measurement/dli-stat-daily']],
                ['label' => '<i class="fa fa-info" aria-hidden="true"></i> Névjegy', 'url' => ['/site/about']],

                ['label' => '<i class="fa fa-wrench" aria-hidden="true"></i> Felhasználókezelés', 'url' => ['/user-management/user/index']],
                ['label' => '<i class="fa fa-wrench" aria-hidden="true"></i> Roles', 'url' => ['/user-management/role/index']],
                ['label' => '<i class="fa fa-wrench" aria-hidden="true"></i> Permissions', 'url' => ['/user-management/permission/index']],
                ['label' => '<i class="fa fa-wrench" aria-hidden="true"></i> Permission groups', 'url' => ['/user-management/auth-item-group/index']],
                ['label' => '<i class="fa fa-wrench" aria-hidden="true"></i> Visit log', 'url' => ['/user-management/user-visit-log/index']],

            ],
        ]);
        ?>
    </section>


</aside>