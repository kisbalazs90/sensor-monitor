<?php app\assets\ChartAsset::register($this) ?>

<textarea class="hidden" name="meassurementData" id="meassurementData" cols="160   " rows="10">
           <?= $meassurements; ?>
</textarea>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="no-padding"><?= $title; ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= Yii::$app->controller->renderPartial('partials/_search', [
                'action' => $action,
                'model' => $searchModel,
                'deviceSelect2List' => $deviceSelect2List,
                'sensorDataSelect2List' => $sensorDataSelect2List
            ]); ?>
        </div>
    </div>
    <div class="row deviceLabels">
        <div class="col-md-12 no-padding">
            <?php if (!empty($deviceTags)): ?>
                <?php foreach ($deviceTags as $data): ?>
                    <span style="background-color:<?= $data['device_color'];?>!important; -webkit-print-color-adjust: exact;  display: block; width: 20px; height: 20px; margin: 0 5px 0 15px; float: left;"></span>
                    <span style="float: left;"><?= $data['device_name'];?></span>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="col-md-9 box-header with-border">
                    <h3 class="box-title">Idődiagramm</h3>
                </div>
                <div class="col-md-3 text-right">
                    <a href="javascript://" id="printLineChart" class="btn btn-default"><i class="fa fa-print" aria-hidden="true"></i></a>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="lineChart" style="height:400px"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p><strong>Össz PAR: <?= $sumOfPar ?> joule/m2</strong></p>
        </div>
    </div>
    <div id="pageBreak"></div>
    <div class="row deviceLabels">
        <div class="col-md-12 no-padding">
            <?php if (!empty($deviceTags)): ?>
                <?php foreach ($deviceTags as $data): ?>
                    <span style="background-color:<?= $data['device_color'];?>!important; -webkit-print-color-adjust: exact;  display: block; width: 20px; height: 20px; margin: 0 5px 0 15px; float: left;"></span>
                    <span style="float: left;"><?= $data['device_name'];?></span>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Oszlopdiagramm</h3>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="barChart" style="height:400px"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $("#w0 > div > div.col-md-1.no-padding > div > button").on("click", function () {
        $('#loaderScreen').removeClass("hidden");
    });

    $("#printLineChart").on("click", function () {
        window.print();
    });

</script>

<!-- LOADER -->
<div id="loaderScreen" class="hidden">
    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
</div>