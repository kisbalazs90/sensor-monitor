<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "measurement".
 *
 * @property int $id
 * @property int $device_id
 * @property string $data1
 * @property string $data2
 * @property string $data3
 * @property string $data4
 * @property string $data5
 * @property string $created_at
 */
class Measurement extends \yii\db\ActiveRecord {

    public $created_at_start;
    public $created_at_end;
    public $type_of_stat;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'measurement';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['device_id', 'created_at', 'type_of_stat'], 'safe'],
                [['data1', 'data2', 'data3', 'data4', 'data5'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'device_id' => Yii::t('app', 'Eszköz'),
            'data1' => Yii::t('app', 'Data1'),
            'data2' => Yii::t('app', 'Data2'),
            'data3' => Yii::t('app', 'Data3'),
            'data4' => Yii::t('app', 'Data4'),
            'data5' => Yii::t('app', 'Data5'),
            'created_at' => Yii::t('app', 'Rögzítve'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevice() {
        return $this->hasOne(Device::className(), ['id' => 'device_id']);
    }

    public function getLastMeasurement($deviceId) {
        return self::find()->where(['device_id' => $deviceId])->orderBy(['created_at' => SORT_DESC])->one();
    }

}
