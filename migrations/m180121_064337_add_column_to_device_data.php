<?php

use yii\db\Migration;

/**
 * Class m180121_064337_add_column_to_device_data
 */
class m180121_064337_add_column_to_device_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('sensor_data','is_disabled', $this->smallInteger(1)->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('sensor_data', 'is_disabled');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180121_064337_add_column_to_device_data cannot be reverted.\n";

        return false;
    }
    */
}
