<?php

use yii\db\Migration;

/**
 * Class m200725_193716_alter_table_device
 */
class m200725_193716_alter_table_device extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('device', 'type', "ENUM('TEMP', 'LIGHT')");
        $this->execute("UPDATE device SET type = 'TEMP'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200725_193716_alter_table_device cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200725_193716_alter_table_device cannot be reverted.\n";

        return false;
    }
    */
}
