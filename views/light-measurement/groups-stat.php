<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\MeasurementSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php app\assets\ChartAsset::register($this) ?>

<textarea class="hidden" name="meassurementData" id="meassurementData" cols="160   " rows="10">
           <?= $meassurements; ?>
</textarea>
<section class="content">
    <div class="row">
        <div class="col-md-12"><h3 class="no-padding"><?= $title; ?></h3></div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <style>
                .s2-togall-button {
                    display: none !important;
                }
            </style>

            <div class="measurement-search">

                <?php
                $form = ActiveForm::begin([
                    'action' => [$action],
                    'method' => 'get',
                ]);
                ?>

                <?php
                $model = new app\models\search\MeasurementSearch();
                ?>

                <div class="row">
                    <div class="col-md-3">
                        <label for="device_id">Mérés típusa</label>
                        <?=
                        $form->field($model, 'type_of_stat')->widget('\kartik\select2\Select2', [
                            'data' => [
                                    'temperature_in' => 'Bejövő hőmérséklet',
                                    'temperature_out' => 'Kimenő hőmérséklet',
                                    'pressure' => 'Bejövő nyomás'
                                ],
                        ])->label(false)
                        ?>
                    </div>
                    <div class="col-md-4">
                        <label for="device_id">Eszközök kiválasztása</label>
                        <?=
                        $form->field($model, 'device_id')->widget('\kartik\select2\Select2', [
                            'data' => $deviceSelect2List,
                            'options' => [
                                'showToggleAll' => false,
                                'placeholder' => 'Válassz...',
                                'multiple' => true,
                            ],
                            'pluginOptions' => [
                                'maximumSelectionLength'=> 30,
                            ],
                        ])->label(false)
                        ?>
                    </div>

                    <div class="col-md-4">
                        <label for="created_at_start">Időpont</label>
                        <?=
                        $form->field($model, 'created_at')->widget('\kartik\datetime\DateTimePicker', [
                                'name' => 'created_at',
                                'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_PREPEND,
                                'value' => date('Y-m-d H:i:s'),
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd hh:ii'
                                ]
                            ]
                        )->label(false)
                        ?>
                    </div>

                    <div class="col-md-1 no-padding">
                        <label for="">&nbsp;</label>
                        <div class="form-group">
                            <?= Html::submitButton('Keresés', ['class' => 'btn btn-primary']) ?>
                        </div>

                    </div>
                </div>
                <?php ActiveForm::end(); ?>

            </div>

        </div>
    </div>
    <div class="row deviceLabels">
        <div class="col-md-12 no-padding">
            <?php if (!empty($deviceTags)): ?>
                <?php foreach ($deviceTags as $data): ?>
                    <span style="background-color:<?= $data['device_color'];?>!important; -webkit-print-color-adjust: exact;  display: block; width: 20px; height: 20px; margin: 0 5px 0 15px; float: left;"></span>
                    <span style="float: left;"><?= $data['device_name'];?></span>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 hidden">
            <div class="box box-info">
                <div class="col-md-9 box-header with-border">
                    <h3 class="box-title">Idődiagramm</h3>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="lineChart" style="height:400px"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-info">
                <div class="col-md-9 box-header with-border">
                    <h3 class="box-title">Oszlopdiagramm</h3>
                </div>
                <div class="col-md-3 text-right">
                    <a href="javascript://" id="printLineChart" class="btn btn-default"><i class="fa fa-print" aria-hidden="true"></i></a>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="barChart" style="height:400px"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $("#w0 > div > div.col-md-1.no-padding > div > button").on("click", function () {
        $('#loaderScreen').removeClass("hidden");
    });

    $("#printLineChart").on("click", function () {
        window.print();
    });
</script>

<!-- LOADER -->
<div id="loaderScreen" class="hidden">
    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
</div>