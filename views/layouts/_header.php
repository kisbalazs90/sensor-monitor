<header class="main-header">
    <a href="/" class="logo">
        <span class="logo-mini"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
        <span class="logo-lg"><b>Sensor</b>Monitor</span>
    </a>
    
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/user-management/auth/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Kijelentkezés </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
